//
//  ViewController.swift
//  UltrApp3000
//
//  Created by Michele Navolio on 20/11/20.
//

import UIKit

class ViewController: UIViewController {

    //sfondoCornice
    @IBOutlet weak var background: UIImageView!
  
    //Schermo e ultra
    @IBOutlet weak var ultra3000: UIImageView!
    @IBOutlet weak var schermo: UIImageView!

    @IBOutlet weak var indicatoreStagione: UIImageView!
    @IBOutlet weak var labelStagione: UILabel!
    
    @IBOutlet weak var indicatorePuntata: UIImageView!
    @IBOutlet weak var labelPuntata: UILabel!
    
    //Bottoni
    @IBOutlet weak var bottoneSu: UIImageView!
    @IBOutlet weak var bottoneCerca: UIImageView!
    @IBOutlet weak var bottoneGiu: UIImageView!
    
    
    @IBOutlet weak var adMob: UIView!
    
    var stato = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        //sono prove per vedere se si vede correttamente, chiaro che manca la logica :D
    
        if stato == 0 {
            ultra3000.image = UIImage(named: "Ultra 3000 accesa")
            schermo.image = UIImage(named: "schermoError")
            if schermo.image == UIImage(named: "schermoError") {
                indicatoreStagione.isHidden = true
                labelStagione.isHidden = true

                indicatorePuntata.isHidden = true
                labelPuntata.isHidden = true

            }
        } else {
            ultra3000.image = UIImage(named: "Ultra 3000 spento")
            schermo.image = UIImage(named: "schermo")

        }
        
    }
    
}

